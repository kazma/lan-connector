package main

import (
	"fmt"
	"io"
	"log"
	"mime"
	"mime/multipart"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/google/uuid"
	"github.com/skip2/go-qrcode"
)

const (
	cssPath = "./css"
	jsPath  = "./js"
)

func main() {
	downloadPath := "./download"
	mustInitDir(downloadPath)
	ms, err := New()
	if err != nil {
		panic(err)
	}

	err = ms.Load()
	if err != nil {
		panic(err)
	}

	app := fiber.New()
	app.Use(recover.New())
	app.Use(logger.New())
	app.Use(requestid.New())

	app.Static("/fs", downloadPath)
	app.Static("/css", cssPath)
	app.Static("/js", jsPath)

	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.SendFile("templates/index.html", true)
	})

	app.Get("/msg", func(ctx *fiber.Ctx) error {
		return ctx.JSON(ms.List())
	})

	app.Post("/msg", func(ctx *fiber.Ctx) error {
		item := Item{}
		if err := ctx.BodyParser(&item); err != nil {
			log.Printf("bind json error %v", err)
			return err
		}

		newItem := NewItem(TEXT, item.Content)
		ms.Add(newItem)
		return ctx.JSON(nil)
	})

	app.Delete("/msg", func(ctx *fiber.Ctx) error {
		id := ctx.Query("id")
		if id == "" {
			return nil
		}
		ms.Remove(id)
		return ctx.JSON(nil)
	})

	app.Get("/qrcode", func(ctx *fiber.Ctx) error {
		content := ctx.Query("content")
		q, err := qrcode.New(content, qrcode.Medium)
		if err != nil {
			return ctx.SendStatus(http.StatusInternalServerError)
		}
		q.DisableBorder = true

		photo, err := q.PNG(256)
		if err != nil {
			return ctx.SendStatus(http.StatusInternalServerError)
		}

		return ctx.Send(photo)
	})

	app.Post("/fs", func(ctx *fiber.Ctx) error {
		fh, err := ctx.FormFile("file")
		if err != nil {
			log.Printf("copy file error %v", err)
			return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to save file"})
		}

		f, err := fh.Open()
		if err != nil {
			log.Printf("read part error %v", err)
			return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to open file"})
		}

		ext := filepath.Ext(fh.Filename)
		randomFileName := fmt.Sprintf("%s%s", uuid.New().String(), ext)
		savePath := filepath.Join(downloadPath, randomFileName)
		out, err := os.Create(savePath)
		if err != nil {
			return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to create file"})
		}

		_, err = io.Copy(out, f)
		if err != nil {
			log.Printf("copy part error %v", err)
			return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to copy file"})
		}

		var t Type
		if isImageFileHeader(fh) {
			t = IMAGE
		} else {
			t = OTHER
		}
		item := NewItem(t, fh.Filename)
		item.Meta = map[string]interface{}{"saveName": randomFileName, "path": downloadPath, "orignanlName": fh.Filename, "fileHeader": fh.Header}
		ms.Add(item)

		return ctx.JSON(fiber.Map{"message": "File uploaded successfully", "filename": savePath})
	})

	app.Get("/download/*", func(ctx *fiber.Ctx) error {
		fp := ctx.Params("*")
		item := ms.Get(fp)
		if item == nil {
			return ctx.SendStatus(http.StatusNotFound)
		}

		path, ok := item.Meta["path"].(string)
		if !ok {
			return ctx.SendStatus(http.StatusNotFound)
		}

		name, ok := item.Meta["saveName"].(string)
		if !ok {
			return ctx.SendStatus(http.StatusNotFound)
		}

		realFilePath := filepath.Join(path, name)

		err := ctx.SendFile(realFilePath)
		if err != nil {
			log.Printf("Send data error %v", err)
			return err
		}

		return nil
	})

	app.Post("/save", func(ctx *fiber.Ctx) error {
		return ms.Save()
	})

	port := 5667
	printEndpointInfo(port)

	log.Printf("run server error: %v", app.Listen(":"+strconv.Itoa(port)))
}

// isImageFileHeader 判断multipart.FileHeader是否为图片
func isImageFileHeader(fh *multipart.FileHeader) bool {
	// 获取MIME类型
	mimeType := mime.TypeByExtension(filepath.Ext(fh.Filename))

	// 常见的图片MIME类型
	imageTypes := []string{
		"image/jpeg",
		"image/png",
		"image/gif",
		"image/bmp",
		// 可以添加其他图片类型
	}

	// 检查MIME类型是否在图片类型列表中
	for _, imageType := range imageTypes {
		if mimeType == imageType {
			return true
		}
	}

	return false
}

func printEndpointInfo(port int) {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Printf("Failed to get interfaces: %v\n", err)
		return
	}

	// 遍历所有网络接口
	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			log.Printf("Failed to get addresses for %s, %v\n", iface.Name, err)
			continue
		}

		// 遍历该接口下的所有IP地址
		for _, addr := range addrs {
			// 检查IP地址是否为IPv4地址
			if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() && ipnet.IP.To4() != nil {
				log.Printf("Local IP address: http://%s:%d\n", ipnet.IP.String(), port)
			}
		}
	}
}

func mustInitDir(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.Mkdir(dir, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
}

func GetContentType(file *os.File) string {
	fileInfo, err := file.Stat()
	if err != nil {
		log.Printf("get file info err %v", err)
		return "application/octet-stream"
	}

	buffer := make([]byte, fileInfo.Size())
	_, err = file.Read(buffer)
	if err != nil {
		log.Printf("read file info err %v", err)
		return "application/octet-stream"
	} else {
		return http.DetectContentType(buffer)
	}
}
