package main

import (
	"bufio"
	"encoding/json"
	"log"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"sync"
	"time"

	"github.com/rs/xid"
)

type Type string

var (
	TEXT  Type = "text"
	IMAGE Type = "image"
	OTHER Type = "other"
)

var CHANGE_LINE = []byte("\n")

type Item struct {
	ID         string                 `json:"id"`
	Type       Type                   `json:"type"`
	Content    string                 `json:"content"`
	Meta       map[string]interface{} `json:"meta"`
	CreateTime time.Time              `json:"create_time"`
}

type IDataOperator interface {
	Add(item *Item)
	Remove(id string)
	Get(id string) *Item
	List() []*Item
	Len() int
}

type IStorage interface {
	Save() error
	Load() error
}

type MemoryStorage struct {
	l           []*Item
	storageFile *os.File
	lock        sync.RWMutex
}

func New() (*MemoryStorage, error) {
	fp := filepath.Join("./download", "metadata")
	log.Printf("MemoryStorage path %s", fp)

	dirPath := filepath.Dir(fp)
	err := os.MkdirAll(dirPath, 0755)
	if err != nil {
		return nil, err
	}

	f, err := os.OpenFile(fp, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		return nil, err
	}

	return &MemoryStorage{
		l:           make([]*Item, 0),
		storageFile: f,
	}, nil
}

func NewItem(t Type, content string) *Item {
	return &Item{
		ID:         xid.New().String(),
		Type:       t,
		Content:    content,
		CreateTime: time.Now(),
	}
}

// IDataOperator

func (s *MemoryStorage) Add(item *Item) {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.l = append(s.l, item)
}

func (s *MemoryStorage) Remove(id string) {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.l = slices.DeleteFunc(s.l, func(item *Item) bool {
		return strings.EqualFold(item.ID, id)
	})
}

func (s *MemoryStorage) Get(id string) *Item {
	s.lock.RLock()
	defer s.lock.RUnlock()

	index := slices.IndexFunc(s.l, func(item *Item) bool {
		return strings.EqualFold(item.ID, id)
	})
	if index == -1 {
		return nil
	}

	return s.l[index]
}

func (s *MemoryStorage) List() []*Item {
	s.lock.RLock()
	defer s.lock.RUnlock()

	return s.l
}

func (s *MemoryStorage) Len() int {
	s.lock.RLock()
	defer s.lock.RUnlock()

	return len(s.l)
}

// IStorage

func (s *MemoryStorage) Load() (err error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	// 重置文件指针到开头
	_, err = s.storageFile.Seek(0, 0)
	if err != nil {
		return err
	}

	// 创建一个scanner来读取文件
	scanner := bufio.NewScanner(s.storageFile)

	// 逐行读取文件内容
	for scanner.Scan() {
		var item Item
		err := json.Unmarshal(scanner.Bytes(), &item)
		if err != nil {
			return err
		}
		s.l = append(s.l, &item)
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func (s *MemoryStorage) Save() error {
	s.lock.RLock()
	defer s.lock.RUnlock()

	// 创建临时文件
	tempFile, err := os.CreateTemp(filepath.Dir(s.storageFile.Name()), "temp_metadata")
	if err != nil {
		return err
	}
	defer func() {
		tempFile.Close()
		os.Remove(tempFile.Name()) // 确保在函数结束时删除临时文件
	}()

	// 使用 bufio.Writer 提高写入效率
	writer := bufio.NewWriter(tempFile)

	// 将列表中的每个项目写入临时文件
	encoder := json.NewEncoder(writer)
	for _, item := range s.l {
		if err := encoder.Encode(item); err != nil {
			return err
		}
	}

	// 刷新缓冲区并关闭临时文件
	if err := writer.Flush(); err != nil {
		return err
	}

	// 重命名临时文件，替换原文件
	if err := os.Rename(tempFile.Name(), s.storageFile.Name()); err != nil {
		return err
	}

	// 重新打开文件以更新 s.storageFile
	newFile, err := os.OpenFile(s.storageFile.Name(), os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		return err
	}
	s.storageFile.Close() // 关闭旧的文件句柄
	s.storageFile = newFile

	return nil
}
